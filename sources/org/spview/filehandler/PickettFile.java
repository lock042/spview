package org.spview.filehandler;

import java.awt.geom.Point2D;

public class PickettFile {

	public static Point2D extractXY(String str) {
		double cx, cy;
		try {
			cx = Double.parseDouble(str.substring(0, 13)); // Double for X

			Double.valueOf(str.substring(13, 21).trim()); // validity test

			cy = Math.pow(10, Double.parseDouble(str.substring(21, 29))); // Double for Y

			Integer.parseInt(str.substring(29, 31).trim()); // validity test
			Double.valueOf(str.substring(31, 41).trim()); // validity test
			Integer.parseInt(str.substring(41, 44).trim()); // validity test
			Double.valueOf(str.substring(44, 51).trim()); // validity test
			Integer.parseInt(str.substring(51, 55).trim()); // validity test
		} catch (NumberFormatException e) { // format error
			return null;
		}
		return (new Point2D.Double(cx, cy));
	}

	public static String extractAssignment(String str) {
		return str.substring(55, 57) + " " + str.substring(57, 59) + " " + str.substring(59, 61) + " " +
				str.substring(67, 69) + " " + str.substring(69, 71) + " " + str.substring(71, 73);
	}
}
