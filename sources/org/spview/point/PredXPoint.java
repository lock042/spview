package org.spview.point;
/**
 * This class allows sorting prediction file X data.
 */
public class PredXPoint implements Comparable<Object> {

    private final double x;                                                // frequency
    private final double y;                                                // intensity
    private final String jsyn;                                             // assignment

    /**
     * Construct a new PredXPoint.
     *
     * @param cx     frequency
     * @param cy     intensity
     * @param cjsyn  prediction assignment
     */
    public PredXPoint(double cx, double cy, String cjsyn) {

        x = cx;                                                      // frequency
        y = cy;                                                      // intensity
        jsyn = cjsyn;                                                // assignment
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get frequency.
     */
    public double getX() {

        return x;
    }

    /**
     * Get intensity.
     */
    public double getY() {

        return y;
    }

    /**
     * Get prediction assignment string.
     */
    public String getJsyn() {

        return jsyn;
    }

    /**
     * Compare frequencies only.
     *
     * @param cpxpt the PredXPoint to compare to
     */
    public int compareTo(Object cpxpt) {

        if( ! (cpxpt instanceof PredXPoint) ) {                      // not the right object
            throw new ClassCastException();
        }
        double delta = ((PredXPoint)cpxpt).getX() - x;               // compare frequencies
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        return 0;                                                    // equal
    }

}
