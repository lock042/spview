package org.spview.preferences;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class CoalescedEventUpdater {
	private Timer timer;

	public CoalescedEventUpdater(int delay, Runnable callback) {
		timer = new Timer(delay, e -> {
			timer.stop();
			callback.run();
		});
	}

	public void update() {
		if (!SwingUtilities.isEventDispatchThread()) {
			SwingUtilities.invokeLater(() -> timer.restart());
		} else {
			timer.restart();
		}
	}
}