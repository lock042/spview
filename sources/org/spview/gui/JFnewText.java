package org.spview.gui;
/*
 * Class to choose a string and return it to a method (PanAff.addExasg or PanAff.setComm)
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/////////////////////////////////////////////////////////////////////

/**
 * Window to define a new string.
 */
public class JFnewText extends    JFrame
                       implements ActionListener {

    /**
	 * 
	 */
    private static final long serialVersionUID = -523507943384721497L;
	private final PanAff calling;                                       // calling PanAff
    private final String type;                                             // string type
    private final String init;                                             // initial string
    private int    ixexp;                                            // exp data index
    private int    iass;                                             // related assignment index

    // buttons
    private JButton jbcancel;
    private JButton jbset;

    // to get the string
    private JTextField jtf;
    private String lnsep;                                            // line separator

/////////////////////////////////////////////////////////////////////

    /**
     * Construct a JFnewText for a new EXASG.
     *
     * @param ccall     calling PanAff
     */
    public JFnewText( PanAff ccall ) {

        super("New EXASG");                                          // main window

        calling = ccall;                                             // calling PanAff
        type    = "exasg";                                           // string type
        init    = "";                                                // initial string

        lnsep = System.getProperty("line.separator");
        setwin();                                                    // set the window
    }

    /**
     * Construct a JFnewText for a comment.
     *
     * @param ccall     calling PanAff
     * @param cinit     initial string
     * @param cixexp    exp data index
     * @param ciass     related assignment index
     */
    public JFnewText( PanAff ccall, String cinit, int cixexp, int ciass ) {

        super("New comment");                                        // main window

        calling = ccall;                                             // calling PanAff
        type    = "comm";                                            // string type
        init    = cinit;                                             // initial string
        ixexp   = cixexp;                                            // exp data index
        iass    = ciass;                                             // related assignment index

        setwin();                                                    // set the window
    }

    // set the window
    private void setwin() {

        addWindowListener(new WindowAdapter() {                      // clean end
            public void windowClosing(WindowEvent e) {
                dispose();                                           // free window
            }
        });

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocation(100,100);                                        // location

        getContentPane().setLayout(new BorderLayout());
        JPanel jp = new JPanel(new BorderLayout());
        getContentPane().add(jp,"Center");

        // panels
        // panels
        JPanel pcentre = new JPanel();
        JPanel psud = new JPanel();

        // center
        jtf = new JTextField(Math.max(30, init.length()));
        jtf.setText(init);
        pcentre.add(jtf);

        // south
        jbcancel = new JButton("Cancel");
        jbcancel.setToolTipText("Cancel text setting");
        jbcancel.addActionListener(this);
        jbset = new JButton("Set");
        jbset.setToolTipText("Set text");
        jbset.addActionListener(this);
        Box boxsud = Box.createHorizontalBox();                      // add buttons to box
        boxsud.add(jbcancel);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbset);
        psud.add(boxsud);

        // insert panels
        jp.add(pcentre,"Center");
        jp.add(psud,   "South");

        pack();
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Process events.
     */
    public void actionPerformed(ActionEvent evt) {

        // cancel
        if( evt.getSource() == jbcancel ) {
            dispose();                                               // free window
            return;
        }
        // accept
        if( evt.getSource() == jbset ) {
            // exasg
            String str;
            if( type.equals("exasg") ) {
                str = jtf.getText().trim();                          // without extra spaces
                if( str.length() > 30 ) {
                    // troncate to 30 chars (FORMAT 3000 of eq_tds.f)
                    int n = JOptionPane.showConfirmDialog(null,"Do you agree to troncate the string to"+lnsep+
                                                               ">>>"+ str.substring(0,30)+"<<<",
                                                               "String too long", JOptionPane.YES_NO_OPTION);
                    if(n == JOptionPane.NO_OPTION) {
                        // nothing to do
                        return;
                    }
                }
                str = str +
                      "          "+
                      "          "+
                      "          ";                                  // complete end of string with spaces
                str = str.substring(0,30);                           // troncate string
                calling.addExasg(str);                               // call method to set exasg
                dispose();                                           // free window
            }
            // comm
            else if( type.equals("comm") ) {
                str = jtf.getText().trim();                          // without extra spaces
                calling.setComm(str, ixexp, iass );                 // call method to set comment
                dispose();                                           // free window
            }
        }
    }

}
